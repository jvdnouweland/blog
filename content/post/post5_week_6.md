---
title: Week 6
date: 2018-03-14
---

Wij zijn met het team aan de tafel gaan zitten om concepten te verzinnen voor het lowfid prototype.
Rick kwam met het idee om de schoolpas te gebruiken om studenten te laten tracken wat zij eten aan eten consumeren op school.
Studenten kunnen dan zien in een wekelijks wat zij aan voedingswaarde consumeren.
Het nadeel van dit is dat vele studenten hun lunch van thuis meenemen of ergens in de buurt hun eten halen.

Om de informatie uit de interviews nuttig te gebruiken zijn wij voor een concept gegaan waar studenten bewust en onbewust beïnvloed worden door media in de Hogeschool Rotterdam.
Zoals studenten die de trap oplopen en zien hoeveel calorieën zij verbranden met de positieve keuze die zij genomen hebben.
Om studenten die normaal de lift nemen te beïnvloeden hebben gekozen voor een interactieve calorieënteller om te laten zien hoeveel calorieën zij mislopen door het nemen van de lift.

Om studenten in de kantine te beïnvloeden willen wij de voedingswaarde bij de producten te zetten en met kleuren aan te geven om te laten zien waar veel vetten, suikers en zouten.
Wij gaan dit in week 7 testen en ik ga beginnen met een lowfid prototype.

Deze week had ik mij origineel ingeschreven voor de workshop “Omgaan met feedback” maar het leek mij een beter idee om de workshop “Rapid prototyping” te volgen omdat ik aan de lowfid prototype ging werken. Deze workshop gaf mij de helderheid dat ik te veel inspanning doe in een lowfid prototype.

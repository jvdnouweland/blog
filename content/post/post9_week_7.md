---
title: Week 15
date: 2018-05-14
---

Maandag 14 mei
In de middag kreeg ik een workshop van Ivo van Vlierden, een oud-student die een soortgelijk onderzoek heeft gedaan in Rotterdam-Zuid.
Tijdens deze workshop lied hij zien hoe hij zijn onderzoek was begonnen en hoe hij de mensen in de buurt is gaan interviewen wat een probleem wat bij vele groepjes.
Wat ik hiervan geleerd heb is dat je gebruik moet maken van het netwerk van de mensen die interviewt om zo in aanraking te komen met de doelgroep en dat je opzoek moet gaan naar een plek waar de doelgroep in een ontspannen omgeving is, zoals op zondag na de moskeedienst want dan vertrekken de mensen die geen tijd hebben en die over blijven zijn meer bereid om in gesprek te gaan over het onderzoek.

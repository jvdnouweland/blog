---
title: Week 7
date: 2018-03-21
---

In het weekend van week 6 heb ik een concept gemaakt van het lowfid prototype om dit maandag te bespreken met mijn projectgroepje.
De feedback die ik kreeg van mijn projectgroep was positief in de zin van dat het duidelijk is wat wij willen bereiken met het concept.

Jantien gaf als feedback op het eerste idee dat het concept van de trap met calorieën niet klopt als je van bijvoorbeeld van verdieping 2 naar 3 loopt in plaats van 1 naar 3. Dit zouden kunnen aanpassen door op een interactieve manier te laten zien hoeveel calorieën de student verbrand met het aantal verdiepingen die hij/zij loopt.

In het lowfid prototype stond op de 5de verdieping een plaatje van een Raketijsje om laten zien dat de gebruiker 40 kcal oftewel een Raketijsje heeft verbrand.
Volgens Jantien oogde 40 kcal meer dan een Raketijsje en heb ik later nagevraagd bij andere studenten om te zien of dit waar is of subjectief. Het resultaat van de 10 gevraagde studenten was dat 7 van de 10 het Raketijsje in hun ogen gezien als meer verbrand of als een prestatie dan de 40 kcal, het Raketijsje wordt sneller geassocieerd als snack en dus ongezond.

Voor de overige concepten was er geen tijd meer om feedback op te krijgen en heb ik dit aan andere studenten gevraagd.

Ik heb deze week geen workshops gevolgd.

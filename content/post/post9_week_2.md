---
title: Week 10
date: 2018-04-18
---

Woensdag 18 april
Vandaag vroegen wij feedback op het Plan van Aanpak, zodat wij de nodige aanpassingen konden doen voor de presentatie op maandag 23 april.
Als feedback kregen wij dat de betrokken nog niet duidelijk aangegeven waren bij de stakeholdersmap, geen duidelijke deadlines, scrum-bord geen planning met tijdlijnen en debrief en aanpak concreter uitbreiden.
Met de feedback die wij gekregen hadden zijn we in de ochtend ver gaan werken aan het Plan van Aanpak.

In de middag heb ik een workshop over gebruikerstesten gevolgd, deze workshop ging over het opstellen van een testplan.

---
title: Week 16
date: 2018-05-29
---

Woensdag 29 mei
Vandaag heb ik samen met mijn projectgroep de testresultaten verwerkt en doorgenomen van de afgelopen twee dagen.
Uit de feedback die wij kregen van Jantine concludeerde wij dat het concept wat wij nu hebben niet realiseerbaar is en meer voor een opleiding in de richting van event management is.
Over het concept zelf van een gezonde markt was ze wel enthousiast en moesten we ook zeker gebruiken, maar dan meer de mensen in de buurt betrekken om zo ook hun culturen met elkaar te verbinden en er meer een onlinemarkt van maken waar mensen recepten van elkaar kunnen zien om gezond eten te stimuleren maar ook om een community te creëren in de buurt.

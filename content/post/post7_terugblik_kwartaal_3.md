---
title: Terugblik Kwartaal 3
date: 2018-03-28
---

Bij mijn vorige leerdossier kreeg ik feedback over dat er groei was in het nemen van initiatief
tijdens de design challenge, hier heb ik dit kwartaal meer op gelet omdat er nog genoeg ruimte was voor verbetering en ik heb deze periode vaker gekozen om deliverables te maken uit eigen initiatief en ik heb mijn projectgroep up-to-date gehouden met welke deliverables ik bezig was.

Ik heb dit kwartaal ook met deliverables gewerkt waar ik in andere kwartalen nog niet mee gewerkt had, waaronder de competentie “Onderzoeken”.

Ik heb dit kwartaal ook meer gebruik gemaakt van de workshops die beschikbaar waren.
En een aantal nieuwe methodes, trucjes geleerd met het zoeken van informatie en bewust geworden dat ik te veel tijd stop in het maken van prototypes waardoor het doel van de low-fid prototypes verliest.

In mijn vrije tijd heb ik dit kwartaal ook tijd besteed om nieuwe vaardigheden te leren zoals Javascript en ben meer gaan tekenen.

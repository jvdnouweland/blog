---
title: Week 12
date: 2018-05-02
---

Woensdag 2 mei (vakantie)
Onderzoek in Carnisse
In de vakantie hebben ik met mijn projectgroep afgesproken om de buurt van Carnisse te verkennen.
Wij zijn begonnen met een rondje door de wijk te lopen en samen bespreken wat er opvalt en hebben dit opgeschreven om later in onze onderzoeksresultaten een conclusie over te schrijven.
Na het verkennen van de buurt zijn opzoek gegaan naar een hotspot om te observeren wat de leeftijd, etniciteit en gezondheid van de mensen die in de buurt wonen.
Na het observeren zijn we naar het winkelcentrum van Zuidplein gegaan om mensen te interviewen, tot onze teleurstelling woonde alle jongeren die wij aanspraken niet in Carnisse en konden wij dus geen vragen stellen over de buurt.
Hierna zijn wij teruggegaan naar de plek waar wij onze eerste observatie hebben gedaan om een tweede keer te observeren om te kijken of er verschil zit in de resultaten op verschillende tijden.
Over het algemeen was het verkennen van de buurt ondanks het niet kunnen interviewen een succes omdat wij nu weten hoe de buurt en de mensen die daar wonen eruitzien.

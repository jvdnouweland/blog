---
title: Week 2
date: 2018-02-14
---

Groepindeling en Plan van aanpak maken met het team.
Vandaag zijn we begonnen met kennismaken en het plan van aanpak ingevuld door samen aan hetzelfde formulier te werken.
Door het invullen van het plan van aanpak is er gelijk een goedbeeld van iedereen z’n sterke punten zijn.
Wij kregen als feedback op het plan van aanpak dat we moesten aangegeven wat onze zwakte punten zijn, de stakeholder map en onderzoeksvragen.

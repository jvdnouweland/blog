---
title: Week 4
date: 2018-02-28
---

Aan het eind van week 3 ben ik donderdag en vrijdag begonnen met het tracken van mijn eten, bezigheden, ontspanning en beweging om dit te verwerken in mijn lifestyle diary.
Ik heb foto’s gemaakt van mijn eten en dit verwerkt in een online voedingstracker om te zien wat voor calorieën, vetten, proteïne en zouten om een goed overzicht te krijgen wat ik nou op een dag eet en verbrand met sporten en bijv. naar school lopen.
Dit heb ik verwerkt in een Illustrator bestand. Dit ga ik woensdag laten valideren om feedback te krijgen.

In deze week had ik mij ingeschreven voor de workshop “Samenwerken” om te kijken of ik hier nieuwe dingen kan leren wat bijdragen aan het samenwerken in mijn projectgroepje.
(Andere verplichten kon ik deze niet volgen maar heb ik gewerkt aan een onderdeel van het project in hetzelfde lokaal waardoor ik nog wel wat kon mee pikken van de workshop).

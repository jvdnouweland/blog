---
title: Week 16
date: 2018-05-28
---

Donderdag 28 mei
Vandaag hebben wij alle producten voor de expo uitgeprint en heb ik het initiatief genomen
Om een high-fid prototype te maken van het nieuwe concept.
Ik heb de huisstijl gebruikt uit de moodboard die ik eerder dit kwartaal heb gemaakt.
Bij het visualiseren van het concept ben ik begonnen met een wireframe om snel een opzet te maken voor de onlineomgeving van de Super-markt.
De high-fid heb ik gemaakt in Sketch, waar ik nog niet eerder mee had gewerkt. Door snel een paar basic tutorials te kijken pakte ik het al snel op en heb voor de hoeveelheid tijd die ik had redelijk goed kunnen verbeelden wat ons concept inhoudt.
Na het afronden van de high fid heb ik een design rationale gemaakt om de designkeuzes die gemaakt zijn te onderbouwen.

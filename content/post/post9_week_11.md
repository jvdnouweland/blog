---
title: Week 16
date: 2018-05-28
---

Dinsdag 28 mei
Wij zijn na het hoorcollege naar de markt bij Blaak gegaan om eerst te observeren welke standjes er zijn, hoeveel van elk er zijn om zo procentueel te zien waar meer aanbod van is.
Na het observeren hebben wij nog een aantal marktbezoekers geïnterviewd over waarom zij naar de markt komen, hoe vaak, voor welke standjes en wat zij van de prijs vonden op de markt.

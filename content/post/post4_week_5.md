---
title: Week 5
date: 2018-03-07
---

Omdat er nog veel onduidelijkheid was over de lifestyle diary is het validatie moment verplaatst naar deze week.
Wel ben ik met mijn groepje begonnen met de lifestyle diary toekomstig.
Wij zijn begonnen met interviews houden om erachter te komen hoe de gebruiker(s) leven waarom zij bepaalde keuzes maken en of zij bewust zijn van bepaalde keuzes.
Uit het onderzoek bleek dat de gebruiker voedingsrichtlijnen leven en aan beweging doen.
Zij nemen bijvoorbeeld de trap omdat zij geen zin hebben om op de lift te gaan wachten en zowel gebruik maken van het openbaarvervoer en lopen/fietsen om naar school te komen.
Wat ons wel opviel was dat gebruikers wel door hadden dat zij onbewust beïnvloed worden media om te gaan sporten of bepaald eten gaan consumeren (o.a. sociale media en advertenties).
Deze informatie kunnen wij weer gebruiken voor het concept waar wij in week 6 en 7 aan gaan werken.
Wij hebben foto’s gemaakt van voeding, beweging en ontspanning voor de huidige lifestyle diary.

Ik had mij voor dinsdag 13 maart ingeschreven voor de workshop “Data visualisatie”, ik vond deze workshop erg matig omdat wij data gingen visualiseren in Excel met behulp van cirkeldiagrammen. Ik heb hier na mijn belevening niks nieuws geleerd en had dus niet de motivatie om mij in te schrijven voor het tweede deel van de workshop “Data visualisatie”.

Ik had mij opgeven voor de workshop “Deskresearch”, omdat ik een onderzoek was gestart naar voeding en beweging.
Aangezien ik in de vorige kwartalen weinig onderzoek had gedaan naar een onderwerp, leek het mij een goed plan om de workshop te volgen voor tips om data op te zoeken maar ook om te analyseren wat relevante data is.

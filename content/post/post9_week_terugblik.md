---
title: Terugblik kwartaal 4
date: 2018-06-03
---

Dit kwartaal heb gereflecteerd naar de feedback die kreeg van mijn projectgenoten bij de peerfeedback.
Ik ben mij meer gaan betrekken in de ontwikkeling van het ontwerpproces.

Bij de inspiratielezingen heb ik veel geleerd over onderzoek doen bij de doelgroep, verder heb ik bij de workshop uitstelgedrag tips gekregen om hieraan te werken.

Ik heb dit kwartaal weer nieuwe dingen geleerd van peers en zag groei in mijn ontwikkeling.

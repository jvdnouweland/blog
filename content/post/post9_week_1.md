---
title: Kickoff Kwartaal 4
date: 2018-04-16
---

Maandag 16 april
Wij zijn vandaag begonnen aan het Plan van Aanpak.
In het Plan van Aanpak hebben wij aangegeven wat ieder zijn sterke en zwakke punten zijn om tijdens het project elkaar scherp te houden.
Wij hebben uitgewerkt waar we de aankomende weken aan kunnen werken om goed op weg te zijn voor het voorbereiden van het onderzoek.

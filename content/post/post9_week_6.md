---
title: Week 14
date: 2018-05-09
---

Woensdag 9 mei
Nu ik klaar ben met de onderzoeksfase, ga ik samen met mijn projectgroep beginnen met ontwikkelen van een concept.
Wij hadden maandag afgesproken dat iedereen met een concept zou aankomen vandaag om onderling discussies te houden over voor concept effectief zou zijn voor Carnisse.
Niemand had een concept waar wij verder mee wouden werken en liepen we dus vast.
Om met nieuwe ideeën te komen zijn we begonnen met het maken van een mindmap.
Al snel kwamen we met ideeën als een kookgroep voor jongeren waar zij gezond leren koken en workshops met sporten om jongeren kennis laten maken met de sportfaciliteiten in de buurt.
Toen we terugkeken naar het onderzoek zagen we dat er weinig mogelijkheden waren om gezond te eten, met dit in ons achterhoofd zijn we gaan kijken naar het combineren van beiden ideeën en het resultaat hiervan was een gezonde markt waar je niet alleen gezonde etenswaren kan kopen maar workshops kan volgen die georganiseerd worden vanuit sportvereniging uit de buurt.

Vandaag heb ik de workshop uitstelgedrag gevolgd omdat dit voor mij een probleem is waar ik al lange tijd mee worstelt.
Van deze workshop heb ik geleerd welke soorten uitstelgedrag er zijn en waar ik onder val.
Uit deze workshop heb ik tips gekregen over dat ik als “Thrill-seeker” een doelen en plannen moet maken en “maak saai, leuk”.
Mijn werk en ontspanning in aparte ruimtes moet doen om zo niet in constante afleiding gebracht worden.
